<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style>        
        .personal_information, .address_information, .payment_information{
            display: none;
        }
        .active{
            display: block;
        }
    </style>
<?php

?>
    <title>Welcome to our app</title>
  </head>
  <body>
    
<div id="app">
     <div class="container">
        
        <div class="mt-5">
            <h1 class="mb-5 text-center">Register Form</h1>
           <form method="post" name="register_form" id="register_form" action="{{url('register')}}"> 
           @csrf 
        
              <div class="personal_information" :class="{active:personal_information}">
                <h3 class="mb-3">Personal Information</h3>
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" name="first_name" class="form-control" placeholder="First Name">
                </div>

                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" name="last_name" class="form-control" placeholder="Last Name">
                </div>

                <div class="form-group">
                    <label>Telephone</label>
                    <input type="tel" name="telephone" class="form-control">
                </div>

                <div class="form-group">
                   <button class="btn btn-primary" @click.prevent="nextForm('address_information')">Next</button>
                </div>
              </div>


              <div class="address_information" :class="{active:address_information}">
                <h3 class="mb-3">Address Information</h3>
                <div class="form-group">
                <label>Street</label>
                <input type="text" name="street" class="form-control" placeholder="Street">
                </div>

                <div class="form-group">
                    <label>House No</label>
                    <input type="text" name="house_no" class="form-control" placeholder="House No">
                </div>

                <div class="form-group">
                    <label>Zip Code</label>
                    <input type="text" name="zip_code" class="form-control">
                </div>

                <div class="form-group">
                    <label>City</label>
                    <input type="text" name="city" class="form-control" placeholder="City">
                </div>   

                <div class="form-group">
                    <button class="btn btn-default" @click.prevent="previousForm('personal_information')">Previous</button>
                    <button class="btn btn-primary" @click.prevent="nextForm('payment_information')">Next</button>
                </div>
              </div>


              <div class="payment_information" :class="{active:payment_information}">
                <h3 class="mb-3">Payment Information</h3>
                
                <div class="form-group">
                    <label>Account Owner</label>
                    <input type="text" name="owner" class="form-control" placeholder="Account Owner">
                </div> 

                <div class="form-group">
                    <label>IBAN</label>
                    <input type="text" name="iban_no" class="form-control" placeholder="IBAN">
                </div>            

                <div class="form-group">
                    <button class="btn btn-default" @click.prevent="previousForm('address_information')">Previous</button>                    
                    <button class="btn btn-primary" name="submit">Submit Payment</button>
                </div>
              </div>
                    
           </form>
        </div>


    </div><!--.container-->

</div>
   
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/vue@2.5.17/dist/vue.js"></script>

    <script>
        var app = new Vue({
            el:'#app',
            data:{
                personal_information:true,
                address_information:false,
                payment_information:false,
            },
            methods:{
                nextForm(next){
                    if (next == 'address_information') {
                        this.personal_information = false;
                        this.address_information = true;                        
                    }
                    if(next == 'payment_information'){
                        this.address_information = false;
                        this.payment_information = true;
                    }
                },

                previousForm(previous){
                    if(previous == 'address_information'){
                        this.payment_information = false;
                        this.address_information = true;
                    }
                    if (previous == 'personal_information') {
                        this.address_information = false;
                        this.personal_information = true;
                    }
                }
            }
        });
    </script>

  </body>
</html>