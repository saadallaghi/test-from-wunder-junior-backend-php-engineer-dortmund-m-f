<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
    	'customer_id', 'owner', 'iban_no',
    ];


    public function user()
    {
    	return $this->belongsTo(User::class);
    }


}
