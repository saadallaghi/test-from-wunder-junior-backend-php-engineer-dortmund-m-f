<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Account;

class RegistrationController extends Controller
{
    public function getRegisterForm()
    {
    	return view('register');
    }

    public function postRegisterData(Request $request)
    {


    	$user = new User;
    	$user->first_name = $request->first_name;
    	$user->last_name = $request->last_name;
    	$user->telephone = $request->telephone;
    	$user->house_no = $request->house_no;
    	$user->street = $request->street;
    	$user->city = $request->city;
    	$user->zip_code = $request->zip_code;
    	$user->save();

    	if($user){
    		$account = $user->accounts()->create([
    			'owner'=>$request->owner,
    			'iban_no'=>$request->iban_no
    		]);
    	}

    	 $url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    	
	    $http =  new \GuzzleHttp\Client([
	    	'headers' => ['Accept' => 'application/json'],
	    	'statusCode'=>200,
	    ]);
	    $response = $http->post($url,[	    	    		
    		'form_params'=>[
    			'customerId'=>$user->id,
    			'iban' =>$account->iban_no,
    			'owner'=>$account->owner,
    		]
	    ]);

	    return json_decode((string) $response->getBody(), true);



    }


}
